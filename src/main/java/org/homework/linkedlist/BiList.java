package org.homework.linkedlist;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

public class BiList<T> implements List<T> {
    private int length;
    private Node<T> head;
    private Node<T> tail;

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> cur = head;

            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                cur = cur.next;
                return val;
            }

            @Override
            public void remove() {
                removeNode(cur);
            }
        };
    }

    public Iterator<T> reversIterator() {
        return new Iterator<T>() {
            Node<T> cur = tail;

            public boolean hasNext() {
                return cur != null;
            }

            public T next() {
                T val = cur.getValue();
                cur = cur.prev;
                return val;
            }

            public void remove() {
                removeNode(cur);
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, length);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1) t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if (head == null) {
            head = tail = new Node<T>().setValue(t);
        } else {
            var node = new Node<T>().setValue(t).setPrev(tail);
            tail.setNext(node);
            tail = node;
        }
        length++;
        return true;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException("Index does not exist");
        if (index == 0) {
            var node = new Node<T>().setValue(element).setNext(head);
            head.setPrev(node);
            head = node;
            length++;
        }
        if (index > 0) {
            Node<T> cur = head;
            for (int i = 0; i < index - 1; i++) cur = cur.getNext();
            var node = new Node<T>().setValue(element).setPrev(cur).setNext(cur.getNext());
            cur.setNext(node);
            length++;
        }
    }

    @Override
    public boolean remove(Object o) {
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                removeNode(cur);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = false;
        for (Object o : c) {
            if (contains(o)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        int ind = index;
        for (T t : c) {
            add(ind, t);
            ind++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : c) {
            if (contains(o)) {
                remove(o);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean delFlag;
        boolean result = false;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            delFlag = false;
            for (Object o : c) {
                if (Objects.equals(cur.value, o)) {
                    delFlag = false;
                    break;
                } else {
                    delFlag = true;
                }
            }
            if (delFlag) {
                removeNode(cur);
                result = true;
            }
        }
        return result;
    }

    @Override
    public void clear() {
        head = tail = null;
        length = 0;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        return cur.getValue();
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        cur.setValue(element);
        return cur.getValue();
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        removeNode(cur);
        return cur.getValue();
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                return index;
            }
            index++;
        }
        return -1;
    }


    @Override
    public int lastIndexOf(Object o) {
        int index = 0;
        int result = -1;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                result = index;
            }
            index++;
        }
        return result;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new ListIterator<T>() {
            Node<T> cur = head;
            int index = 0;

            @Override
            public boolean hasNext() {
                return (cur != null);
            }

            @Override
            public T next() {
                T val = cur.getValue();
                cur = cur.next;
                index++;
                return val;
            }

            @Override
            public boolean hasPrevious() {
                return cur.prev != null;
            }

            @Override
            public T previous() {
                T val = cur.prev.getValue();
                cur = cur.prev;
                index--;
                return val;
            }

            @Override
            public int nextIndex() {
                if (cur == tail) {
                    return length;
                } else {
                    return index + 1;
                }
            }

            @Override
            public int previousIndex() {
                return index - 1;
            }

            @Override
            public void remove() {
                removeNode(cur);
            }

            @Override
            public void set(T t) {
                cur.setValue(t);
            }

            @Override
            public void add(T t) {
                var node = new Node<T>().setValue(t).setNext(head);
                if (head == null && cur == head) {
                    head = tail = cur = new Node<T>().setValue(t);

                } else if (cur == head) {
                    node.setValue(t).setNext(head);
                    head.setPrev(node);
                    head = node;

                } else if (cur.prev != null) {
                    Node<T> cur = head;
                    for (int i = 0; i < index - 1; i++) cur = cur.getNext();
                    node.setValue(t).setPrev(cur).setNext(cur.getNext());
                    cur.setNext(node);
                }
                length++;
            }
        };
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        ListIterator<T> iterator = this.listIterator();
        for (int i = 0; i < index; i++) iterator.next();
        return iterator;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex > length || fromIndex > toIndex) throw new IndexOutOfBoundsException();
        List<T> list = new BiList<>();
        for (int i = fromIndex; i < toIndex; i++) list.add(get(i));
        return list;
    }

    @Data
    @Accessors(chain = true)
    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;
    }


    private void removeNode(Node<T> cur) {
        if (cur == head && cur == tail) {
            clear();
        } else {
            if (cur.prev != null) cur.prev.setNext(cur.next);
            if (cur.next != null) cur.next.setPrev(cur.prev);

            if (cur == head) head = cur.next;
            else if (cur == tail) tail = cur.prev;

            length--;
        }
    }
}
