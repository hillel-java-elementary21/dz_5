package org.homework.linkedlist;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

public class SingleList<T> implements List<T> {
    private int length;
    private Node<T> head;
    private Node<T> tail;

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (SingleList.Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> cur = head;
            Node<T> prev;

            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                prev = cur;
                cur = cur.next;
                return val;
            }

            @Override
            public void remove() {
                if (cur == head && cur == tail) {
                    clear();
                } else {
                    if (prev != null) prev.setNext(cur.next);

                    if (cur == head) head = cur.next;
                    else if (cur == tail) tail = prev;

                    length--;
                }
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, length);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1) t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if (head == null) {
            head = tail = new Node<T>().setValue(t);
        } else {
            var node = new Node<T>().setValue(t);
            tail.setNext(node);
            tail = node;
        }
        length++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
       int delIndex = indexOf(o);
            if (delIndex != -1) {
                remove(delIndex);
                return true;
            }
            return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = false;
        for (Object o : c) {
            if (contains(o)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        int ind = index;
        for (T t : c) {
            add(ind, t);
            ind++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : c) {
            if (contains(o)) {
                remove(o);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean delFlag;
        boolean result = false;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            delFlag = false;
            for (Object o : c) {
                if (Objects.equals(cur.value, o)) {
                    delFlag = false;
                    break;
                } else {
                    delFlag = true;
                }
            }
            if (delFlag) {
                remove(cur.value);
                result = true;
            }
        }
        return result;
    }

    @Override
    public void clear() {
        head = tail = null;
        length = 0;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        return cur.getValue();
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        cur.setValue(element);
        return element;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException("Index does not exist");
        if (index == 0) {
            head = new Node<T>().setValue(element).setNext(head);
            length++;
        }
        if (index > 0) {
            SingleList.Node<T> cur = head;
            for (int i = 0; i < index - 1; i++) cur = cur.getNext();
            var node = new Node<T>().setValue(element).setNext(cur.getNext());
            cur.setNext(node);
            length++;
        }
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        T value = null;

        if (index == 0) {
            value = cur.value;
            head = cur.next;
        }
        if (index > 0 && index < length - 1) {
            for (int i = 0; i < index - 1; i++) cur = cur.getNext();
            value = cur.next.getValue();
            cur.setNext(cur.next.next);

        } else if (index == length - 1) {
            for (int i = 0; i < index - 1; i++) cur = cur.getNext();
                value = tail.getValue();
                cur.setNext(null);
                tail = cur;
        }
        length--;

        return value;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = 0;
        int result = -1;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                result = index;
            }
            index++;
        }
        return result;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex > length || fromIndex > toIndex) throw new IndexOutOfBoundsException();
        List<T> list = new BiList<>();
        for (int i = fromIndex; i < toIndex; i++) list.add(get(i));
        return list;
    }

    @Data
    @Accessors(chain = true)
    private static class Node<T> {
        private T value;
        private Node<T> next;
    }
}

