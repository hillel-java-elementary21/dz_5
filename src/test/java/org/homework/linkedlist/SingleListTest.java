package org.homework.linkedlist;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SingleListTest {
    @Test
    @DisplayName("Добавление и получение по индексу")
    public void testAddAndGet() {
        List<Integer> lst = new SingleList<>();
        assertTrue(lst.isEmpty());
        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        assertFalse(lst.isEmpty());

        assertEquals(4, lst.size());
        assertEquals(10, lst.get(0));
        assertEquals(20, lst.get(1));
        assertEquals(60, lst.get(2));
        assertEquals(80, lst.get(3));
    }

    @Test
    @DisplayName("Проверка итератора")
    public void testIterator() {
        List<String> list = new SingleList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        StringBuilder res = new StringBuilder();
        for (String s : list) {
            res.append(s);
        }
        assertEquals("abcd", res.toString());
        List<Integer> list2 = new SingleList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        StringBuilder res2 = new StringBuilder();
        for (Integer s : list2) {
            res2.append(s);
        }
        assertEquals("1234", res2.toString());
    }

    @Test
    @DisplayName("Преобразование в массив")
    public void testToArray() {
        List<Integer> list = new SingleList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);

        Integer[] arr1 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{10, 20, 60, 80}, arr1);

        Object[] arr2 = list.toArray();
        assertArrayEquals(new Object[]{10, 20, 60, 80}, arr2);
    }

    @Test
    @DisplayName("Удаление итератор")
    public void testRemove() {
        List<Integer> list = new SingleList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        //remove head
        Iterator<Integer> it0 = list.iterator();
        it0.remove();
        assertEquals(4, list.size());
        assertArrayEquals(new Integer[]{20, 60, 80, 100}, list.toArray(new Integer[0]));

        //remove second
        Iterator<Integer> it1 = list.iterator();
        it1.next();
        it1.remove();
        assertEquals(3, list.size());
        assertArrayEquals(new Integer[]{20, 80, 100}, list.toArray(new Integer[0]));

        //remove tail
        Iterator<Integer> it2 = list.iterator();
        it2.next();
        it2.next();
        it2.remove();
        assertEquals(2, list.size());
        assertArrayEquals(new Integer[]{20, 80}, list.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Удаление по значению")
    public void testRemoveValue() {
        List<Integer> lst = new SingleList<>();
        lst.add(80);
        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);

        assertTrue(lst.remove(Integer.valueOf(80)));
        assertArrayEquals(new Integer[]{10, 20, 60, 80, 100}, lst.toArray(new Integer[0]));

        assertFalse(lst.remove(Integer.valueOf(90)));
        assertArrayEquals(new Integer[]{10, 20, 60, 80, 100}, lst.toArray(new Integer[0]));
        //remove head
        assertTrue(lst.remove(Integer.valueOf(10)));
        assertArrayEquals(new Integer[]{20, 60, 80, 100}, lst.toArray(new Integer[0]));
        //remove tail
        assertTrue(lst.remove(Integer.valueOf(100)));
        assertArrayEquals(new Integer[]{20, 60, 80,}, lst.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Удаление по индексу")
    public void testRemoveIndexValue() {
        List<Integer> lst = new SingleList<>();
        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);

        assertEquals(80, lst.remove(3));
        assertArrayEquals(new Integer[]{10, 20, 60, 100}, lst.toArray(new Integer[0]));

        assertEquals(10, lst.remove(0));
        assertArrayEquals(new Integer[]{20, 60, 100}, lst.toArray(new Integer[0]));

        assertEquals(100, lst.remove(2));
        assertArrayEquals(new Integer[]{20, 60}, lst.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Добавление по индексу")
    public void testAddIndexValue() {
        List<Integer> lst = new SingleList<>();
        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);

        lst.add(0, -10);
        assertArrayEquals(new Integer[]{-10, 10, 20, 60, 80, 100}, lst.toArray(new Integer[0]));

        lst.add(2, -60);
        assertArrayEquals(new Integer[]{-10, 10, -60, 20, 60, 80, 100}, lst.toArray(new Integer[0]));

        lst.add(4, -100);
        assertArrayEquals(new Integer[]{-10, 10, -60, 20, -100, 60, 80, 100}, lst.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Добавление коллекции")
    public void testAddALl() {
        List<Integer> lst = new SingleList<>();
        List<Integer> lst2 = new SingleList<>();
        List<Integer> lst3 = new SingleList<>();
        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);
        lst2.add(-30);
        lst2.add(-40);
        lst2.add(-50);
        lst2.add(-60);
        lst3.add(123);

        lst.addAll(lst2);
        assertArrayEquals(new Integer[]{10, 20, 60, 80, 100, -30, -40, -50, -60}, lst.toArray(new Integer[0]));

        lst.addAll(lst3);
        assertArrayEquals(new Integer[]{10, 20, 60, 80, 100, -30, -40, -50, -60, 123}, lst.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Добавление коллекции по индексу")
    public void testIndexAddALl() {
        List<Integer> list = new SingleList<>();
        List<Integer> list2 = new SingleList<>();
        List<Integer> list3 = new SingleList<>();

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);
        list2.add(-30);
        list2.add(-40);
        list2.add(-50);
        list2.add(-60);
        list3.add(66);
        list3.add(77);

        list.addAll(0, list2);
        assertArrayEquals(new Integer[]{-30, -40, -50, -60, 10, 20, 60, 80, 100}, list.toArray(new Integer[0]));

        list2.addAll(3, list3);
        assertArrayEquals(new Integer[]{-30, -40, -50, 66, 77, -60}, list2.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Проверка содержет ли объект")
    public void testContain() {
        List<String> list = new SingleList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("abc");
        assertFalse(list.contains("e"));
        assertFalse(list.contains(10));
        assertTrue(list.contains("a"));
        assertTrue(list.contains("d"));
        assertTrue(list.contains("abc"));
    }

    @Test
    @DisplayName("Проверка содержет ли коллекцию")
    public void testContainCollection() {
        List<String> list = new SingleList<>();
        List<String> list2 = new SingleList<>();
        List<String> list3 = new SingleList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("abc");

        list2.add("a");
        list2.add("d");
        list2.add("b");

        list3.add("a");
        list3.add("e");
        list3.add("c");

        assertTrue(list.containsAll(list2));
        assertFalse(list.containsAll(list3));
        assertTrue(list.containsAll(list));
        assertFalse(list2.containsAll(list));
    }

    @Test
    @DisplayName("Индекс первого вхождения")
    public void testIndexOf() {
        List<String> list = new SingleList<>();
        List<Integer> list2 = new SingleList<>();

        list.add("a");
        list.add("b");
        list.add("a");
        list.add("d");
        list.add("abc");

        list2.add(12);
        list2.add(-10);
        list2.add(13);
        list2.add(13);

        assertEquals(0, list.indexOf("a"));
        assertEquals(-1, list.indexOf("e"));
        assertEquals(4, list.indexOf("abc"));

        assertEquals(0, list2.indexOf(12));
        assertEquals(-1, list2.indexOf(25));
        assertEquals(2, list2.indexOf(13));
    }

    @Test
    @DisplayName("Индекс последнего вхождения")
    public void testLastIndexOf() {
        List<String> list = new SingleList<>();
        List<Integer> list2 = new SingleList<>();

        list.add("a");
        list.add("b");
        list.add("a");
        list.add("d");
        list.add("abc");

        list2.add(12);
        list2.add(12);
        list2.add(-10);
        list2.add(13);
        list2.add(13);
        list2.add(13);

        assertEquals(2, list.lastIndexOf("a"));
        assertEquals(-1, list.lastIndexOf("e"));
        assertEquals(4, list.lastIndexOf("abc"));

        assertEquals(1, list2.lastIndexOf(12));
        assertEquals(-1, list2.lastIndexOf(25));
        assertEquals(5, list2.lastIndexOf(13));
    }

    @Test
    @DisplayName("Удаление коллекции")
    public void testRemoveAll() {
        List<Integer> lst = new SingleList<>();
        List<Integer> lst2 = new SingleList<>();
        List<Integer> lst3 = new SingleList<>();
        List<Integer> lst4 = new SingleList<>();

        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);

        lst2.add(10);
        lst2.add(60);
        lst2.add(80);

        lst3.add(-1);
        lst3.add(-2);

        lst4.add(20);
        lst4.add(-2);
        lst4.add(-2);

        assertTrue(lst.removeAll(lst2));
        assertArrayEquals(new Integer[]{20, 100}, lst.toArray(new Integer[0]));

        assertFalse(lst.removeAll(lst3));
        assertArrayEquals(new Integer[]{20, 100}, lst.toArray(new Integer[0]));

        assertTrue(lst.removeAll(lst4));
        assertArrayEquals(new Integer[]{100}, lst.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Удаление вне коллекции")
    public void testRetainAll() {
        List<Integer> lst = new SingleList<>();
        List<Integer> lst2 = new SingleList<>();
        List<Integer> lst3 = new SingleList<>();
        List<Integer> lst4 = new SingleList<>();

        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);

        lst2.add(10);
        lst2.add(60);
        lst2.add(80);

        lst3.add(-1);
        lst3.add(-2);

        lst4.add(80);
        lst4.add(-2);
        lst4.add(-2);

        assertTrue(lst.retainAll(lst2));
        assertArrayEquals(new Integer[]{10, 60, 80}, lst.toArray(new Integer[0]));

        assertTrue(lst.retainAll(lst4));
        assertArrayEquals(new Integer[]{80}, lst.toArray(new Integer[0]));

        assertTrue(lst.retainAll(lst3));
        assertArrayEquals(new Integer[]{}, lst.toArray(new Integer[0]));

        assertFalse(lst2.retainAll(lst2));
        assertArrayEquals(new Integer[]{10, 60, 80}, lst2.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Проверка сублиста")
    public void testSubList() {
        List<Integer> lst = new SingleList<>();

        lst.add(10);
        lst.add(20);
        lst.add(60);
        lst.add(80);
        lst.add(100);

        assertArrayEquals(new Integer[]{10, 20, 60, 80}, lst.subList(0, 4).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{20, 60, 80}, lst.subList(1, 4).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{10}, lst.subList(0, 1).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{80}, lst.subList(3, 4).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{}, lst.subList(0, 0).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{}, lst.subList(4, 4).toArray(new Integer[0]));
    }

}